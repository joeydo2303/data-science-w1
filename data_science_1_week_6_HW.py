import numpy as np
import pandas as pd
df_bus = pd.read_csv('Potentail_Bust_Stops.csv')
df_bus.head(5)

!pip install Folium==0.5.0
import folium

latitude = 37.77
longitude = -122.42
sanfran_map = folium.Map(location=[latitude, longitude], zoom_start=12)

incidents = folium.map.FeatureGroup()
for lat, lng, in zip(df_bus.Street_One, df_bus.Street_Two):
    incidents.add_bus_mark(
        folium.features.CircleMarker(
            [lat, lng],
            radius=5, # define how big you want the circle markers to be
            color='yellow',
            fill=True,
            fill_color='blue',
            fill_opacity=0.6
        )
    )
    
sanfran_map.add_child(incidents)